<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Learn making a website</title>
</head>
<body bgcolor="yellow" link="red" vlink="maroon">
<h1><b><u><center>Make Your Own Website</center></u></b></h1><hr/>
<br/><br/>
<font face="arial" size="auto"><p>Everything on the World Wide Web(WWW or web for short) is specially designed.<br/>
Websites are created in a coding language called<b> HTML</b>. HTML stands for <b>"HyperText Markup Language"</b>. Hypertext refers to text viewable on the web.</p>
<p>Nowadays, a new version of HTML, HTML5 is used. HTML did not support vedios and audios. HTML5 allows this.<br/>Html uses ‘tags’ to define tasks. Html tags are denoted by "<" & ">". For eg.: <textarea rows="1" cols="6"><html></textarea>defines the begining of a web page. For ending html tags, we use "/" within"<>". For eg.:<font size="3"><textarea cols="7" rows="1"></html> </textarea> determines the end of your webpage.</p></font>
<p>HTML documents can be made in any text editing software or html coding application. For making a text document an html file, save it with the extension: .html (for eg.: untitled1.html)</p>
<p>Lets look at some basic tags in HTML5 in the order they are used:<BR/><font>
<textarea cols="77" rows="11" align="center"><!DOCTYPE html>: This defines the begining of an HTML5 document.
<head>: This will help you give a name to the webpage.
<title>: This exactly names the webpage.
</title>: This marks the end of the "title" tag.
</head>: This marks the end of the "head" tag
<body>: This defines the start of the viewable part of the webpage.
<br>: The text typed after this is shown from a new line. This is a tag which does not require the closing tag
</body>: This marks the end of the viewable part of the webpage.
</html>: This marks the end of the webpage.
</textarea></font><br/>The Doctype declaration before the html tag is the declaration of the document to be an HTML5 document.</p>
<p>Now, see this example code(you may even copy it):<center>
<textarea cols="60" rows="11">
<!DOCTYPE html>
<head>
<title>This is an exampler webpage.</title>
</head>
<body>
Hello Viewers! This is an example.
<br>
This is the "br" tag.
</body>
</html>
</textarea></center></p>
<p>Now save this file with .html format.(I saved it as example.html)<br/>Then open the file in your browser. For eg.: a file made on notepad and saved with .html extension, when opened, will directly open in browser. If you have made your file in an html coder, then run the module(it may be done by the "play"/"browser" icon.) This will give the following output:<br/></p><center>
<img alt="output" border="1" bordercolor="black" height="350" src="https://1.bp.blogspot.com/-5eqd1JYEx1o/X6jdjH5iIZI/AAAAAAAAACE/FLY00XKCNYcI0MSvPzdzTaO8eZJqYVRzwCLcBGAsYHQ/s0/6F1D0F0E-5A9A-491D-8D47-F3886EA47886.jpeg" width="75%"/></center><hr/><br/><p></p></font>
<table border="3" bordercolor="gray" bgcolor="black" align="center"><tr><th> <font color="white"><center>
<h3>This was just an introduction. For learning more, refer to the links below:</h3></align></center><br></th></tr><tr><td><font color="white">1.
<a href="body.html">Making your webpage more beautiful.</a><br/>2.
<a href="img.html">Inserting images in a webpage</a><br/>3.
<a href="links.html">Inserting links in a webpage</a><br/>4.
<a href="lists.html">Inserting lists in a webpage</a><br/>5.
<a href="tables.html">Inserting tables in a webpage</a><br/>6.
<a href="marquee.html">Inserting moving text(marqee) in a webpage</a><br/>7.
<a href="forms.html">Creating Forms in HTML</a><br/>8.
<a href="ved-aud.html">Inserting videos and audios in a webpage</a><br/><br/>
<br/></font></td></tr><tr><td></font>
<font size="1" color="white"><center>By- Soham Kakkar</font></center></td>
 </tr></table></center>
</body>
</html>